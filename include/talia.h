#ifndef TALIA_H
#define TALIA_H

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>

using namespace std;

#include "gracz.h"
#include "karta.h"

/*
 * Klasa talii
 */

class Talia
{
    public:
        Talia();
        virtual ~Talia();
        void wypisz();
        void wypisz(Gracz&);
        void wydaj(Gracz&);
        void dodaj();
        void tasuj();
    protected:
    private:
        vector <Karta> karty;

};

#endif // TALIA_H
