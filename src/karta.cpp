#include "karta.h"

Karta::Karta(short kolor, short numer) {
    this->kolor = kolor;
    this->numer = numer;
}

string Karta::pobierzKolor() {
    switch (this->kolor) {
    case 0:
        return "\x5";
    case 1:
        return "\x4";
    case 2:
        return "\x3";
    case 3:
        return "\x6";
    }
}

string Karta::pobierzNumer() {
    switch (this->numer) {
    case 1:
        return "As";
    case 11:
        return "Walet";
    case 12:
        return "Dama";
    case 13:
        return "Krol";
    }
    ostringstream ss;
    ss << this->numer;
    return ss.str();
}

Karta::~Karta() {
    //dtor
}
