#include "gra.h"

Gra::Gra() {
    cout << "Witaj w grze karcianej" << endl;
    cout << "Wybierz ilu ma byc graczy (max 6):";
    cin >> this->ilosc_graczy;
    if (this->ilosc_graczy > 6 || this->ilosc_graczy <= 0) {
        cout << "Niestety ale podana liczba (" << this->ilosc_graczy << ") jest bledna!";
        return;
    }

    this->stworzGraczy();
    this->stworzTalie();
    this->przetasujTalie();

    this->wydajKarty();

    this->wyswietlKartyGraczy();
    this->wyswietlKartyNaStole();
}

void Gra::stworzGraczy() {
    for (int i = 0; i < this->ilosc_graczy; i++) {
        this->Gracze.push_back(Gracz());
    }
}

void Gra::stworzTalie() {
    this->Talia_graczy = Talia();
}

void Gra::przetasujTalie() {
    this->Talia_graczy.tasuj();
}

void Gra::wydajKarty() {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < this->ilosc_graczy; j++) {
            this->Talia_graczy.wydaj(this->Gracze[j]);
        }
    }
}

void Gra::wyswietlKartyGraczy() {
    for (int i = 0; i < this->ilosc_graczy; i++) {
        this->Talia_graczy.wypisz(this->Gracze[i]);
    }
}

void Gra::wyswietlKartyNaStole() {
    this->Talia_graczy.wypisz();
}

Gra::~Gra() {
    //dtor
}
